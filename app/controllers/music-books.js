import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
        createMusic(){
        this.store.createRecord('music', {
          name: this.get('name'),
          description: this.get('description')
        }).save().then(()=>{
          this.set('name', '')
          this.set('description', '')
          alert('La música ha sido guardada')
        })
      },
      deleteMusic(music){
        music.destroyRecord().then(()=>{
          alert("La música ha sido eliminada")
        })
      },
      updateMusic(music){
        music.save().then(()=>{
          alert("La música ha sido actualizada")
        })
      }
      }

});
